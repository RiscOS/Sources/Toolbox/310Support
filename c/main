/* Copyright 1994 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* Title:   main.c
 * Purpose: 3.10 support
 * Author:  NK
 *
 */

#define IGNORE(a) a=a

#include <stdio.h>
#include <stdlib.h>
#include "kernel.h"
#include "swis.h"
#include "string.h"

#include "Global/Services.h"

#include "tboxlibint/objects/window.h"
#include "tboxlibint/rmensure.h"
#include "ThreeTenHdr.h"

extern _kernel_oserror *_load_sprites(_kernel_swi_regs *r, void *pw)
{
    _kernel_swi_regs regs;
    char buf[64] = "system:modules.310support.icons";

    regs.r[0] = 2;

    _kernel_swi(Wimp_ReadSysInfo,&regs,&regs);
    strcat (buf, (char *) regs.r[0]);

    regs.r[0] = 11;
    regs.r[2] = (int) buf;

    if (_kernel_swi(Wimp_SpriteOp,&regs,&regs)) {
       strcpy(buf,"system:modules.310support.icons");
       regs.r[0] = 11;
       regs.r[2] = (int) buf;
       _kernel_swi(Wimp_SpriteOp,&regs,&regs);
    }
    IGNORE(r);
    IGNORE(pw);

    return NULL;

}

static void load_sprites(void *pw)
{
    _kernel_swi_regs regs;

    regs.r[0] = (int) register_cb;
    regs.r[1] = (int) pw;
    _kernel_swi(OS_AddCallBack,&regs,&regs);
}

extern _kernel_oserror *threeten_init(const char *cmd_tail, int podule_base, void *pw)
{

    IGNORE(cmd_tail);
    IGNORE(podule_base);

    /* draggables need DAS 0.10 as 3.10 DAS doesn't work properly */

    rmensure ("DragASprite","DragASprit","0.10");

    /* for window deletion */
    rmensure ("BorderUtils","BorderUtil","0.05");
    _load_sprites(NULL, pw);

    return NULL;
}

/* +++++++++++++++++++++++++++++++++ service handler code ++++++++++++++++++++++++++++++ */


extern void threeten_services(int service_number, _kernel_swi_regs *r, void *pw)
{
    IGNORE(r);

    switch (service_number)
    {

        case Service_ModeChange:
          {
            load_sprites(pw);
            break;
          }
        default:
            break;
    }

}

/* ++++++++++++++++++++++++++++++++++++++ SWI code +++++++++++++++++++++++++++++++++++++ */

static void fudge_icon(int window, int icon, int flags, int new)
{
  wimp_SetIconState  set ;
  _kernel_swi_regs regs;

  set.window_handle = window;
  set.icon_handle   = icon ;
  set.clear_word    = flags ;
  set.EOR_word      = new;

  regs.r[1] = (int) &set;

  _kernel_swi(Wimp_SetIconState,&regs,&regs);
}

static void fade_icon(int window, int icon, int state)
{
  fudge_icon(window,icon,wimp_ICONFLAGS_FADED ,state ? wimp_ICONFLAGS_FADED : 0);
}

/* we do not fade labels, for radios and options we only fade the sprite */

static void fade_gadget(GadgetHeader * hdr, int *icons, int window, int fade)
{
   switch(hdr->type) {
      case LabelledBox_Base:
         if (hdr->flags & LabelledBox_Sprite) {
            fade_icon(window,icons[0],fade);
            break;
         }
         /* no break for text */
      case Label_Base:
         fudge_icon(window,icons[0],wimp_ICONFLAGS_FORECOL * 15,
             fade ? wimp_ICONFLAGS_FORECOL * 3 : wimp_ICONFLAGS_FORECOL * 7);
         break;
      case OptionButton_Base:
      case RadioButton_Base:
         fade_icon(window,icons[1],fade);
         fudge_icon(window,icons[0],wimp_ICONFLAGS_BUTTON_TYPE * 15,
             fade ? 0 : wimp_ICONFLAGS_BUTTON_TYPE * wimp_BUTTON_CLICK );
         break;

      case Slider_Base:
         fade_icon(window,icons[1],fade);
         fade_icon(window,icons[2],fade);
                /* deliberate no break */
      default:
         fade_icon(window,*icons,fade);

         break;

   }
}

extern _kernel_oserror *threeten_SWI_handler(int swi_no, _kernel_swi_regs *r, void *pw)
{
    _kernel_swi_regs regs;
    IGNORE(pw);

    switch (swi_no)
    {
        case 0:
           /* fade gadget */
            fade_gadget(((GadgetHeader *) r->r[0]), (int *) r->r[1], r->r[2], r->r[3]);
            break;
        case 1:
           /* delete window BU call */
           regs.r[0] = 5;
           _kernel_swi(Wimp_ReadSysInfo,&regs,&regs);
           regs.r[2] = regs.r[0];
           regs.r[0] = r->r[0];
           regs.r[1] = 0x44ec5;
           _kernel_swi(OS_ServiceCall,&regs,&regs);
           break;
        default:
           return error_BAD_SWI;
    }

    return NULL;
}
