# Copyright 1994 Acorn Computers Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for 310Support
#

COMPONENT   = 310Support
OBJS        = main
HDRS        =
CMHGDEPENDS = main
CMHGFILE    = ThreeTenHdr
LIBS       += ${TBOXINTLIB}
CUSTOMRES   = no
CUSTOMSA    = custom

include CModule

# Sprites are expected alongside the module
install_custom: ${SA_TARGET_RULE}
	${MKDIR} ${INSTDIR}
	${CP} ${SA_TARGET_RULE} ${INSTDIR}${SEP}${TARGET}${SUFFIX_MODULE} ${CPFLAGS}
	${INSTRES} -I Resources ${INSTDIR} icons icons22
	@${ECHO} ${COMPONENT}: ram module installed

standalone_custom: ${SA_TARGET_RULE}
	@${ECHO} ${COMPONENT}: ram module built

# Dynamic dependencies:
